## 安装

```bash
npm install
```

## 使用

### 使用 node 运行

```bash
yarn start
```

### 使用浏览器运行

```bash
yarn dev
```
